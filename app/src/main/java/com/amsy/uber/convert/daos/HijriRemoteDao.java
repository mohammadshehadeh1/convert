package com.amsy.uber.convert.daos;

import com.amsy.uber.convert.model.Home.DateWrapperRemote;
import com.amsy.uber.convert.network.HttpHelper;
import com.amsy.uber.convert.network.MyCall;


import retrofit2.http.GET;
import retrofit2.http.Query;

public class HijriRemoteDao {
    private static HijriRemoteDao instance;
    private DashboardClient dashboardClient;

    public HijriRemoteDao() {
        dashboardClient = HttpHelper.getInstance().create(DashboardClient.class);
    }


    public  static  synchronized HijriRemoteDao getInstance(){
        if(instance ==null)
            instance=new HijriRemoteDao();
        return instance;
    }
    public MyCall<DateWrapperRemote> getData(String date ) {
        return dashboardClient.getData(date);
    }

    private interface DashboardClient {
        @GET("gToH")
        public MyCall<DateWrapperRemote> getData(@Query("date") String date);
    }
}
    //@GET("v3/kitchen/recipes/{id}")
    //public MyCall<InfoRecipes> getInfoRecipes(@Path("id")Integer id);
