package com.amsy.uber.convert.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.amsy.uber.convert.model.Event;

@Database(entities = Event.class ,version = 6, exportSchema = false)
public abstract class MyDataBase extends RoomDatabase {
    public  abstract EventDao postDao();
}
