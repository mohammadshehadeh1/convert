package com.amsy.uber.convert.room;

import android.app.Application;

import androidx.room.Room;

public class MyApplication extends Application {
    private static MyDataBase instanceDB;
    private static MyApplication appInstance;

    @Override
    public void onCreate() {
        super.onCreate();


        if (appInstance == null)
            appInstance = this;

        if (instanceDB == null) {
            instanceDB = Room.databaseBuilder(this, MyDataBase.class, "M7mad").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        }


    }

    public static MyDataBase getInstanceDB() {
        return instanceDB;
    }

    public static synchronized MyApplication getAppInstance() {
        return appInstance;
    }
}
