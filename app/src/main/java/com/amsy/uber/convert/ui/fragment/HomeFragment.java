package com.amsy.uber.convert.ui.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.amsy.uber.convert.R;
import com.amsy.uber.convert.daos.HijriRemoteDao;
import com.amsy.uber.convert.databinding.FragmentHomeBinding;
import com.amsy.uber.convert.model.Home.DateWrapperRemote;
import com.amsy.uber.convert.model.Home.Gregorian;
import com.amsy.uber.convert.network.HttpStatus;

import java.util.Calendar;

public class HomeFragment extends Fragment {

    FragmentHomeBinding binding;
    DatePickerDialog.OnDateSetListener listener;
    private int year;
    private int month;
    private int day;
    private String gregorian;
    private String dateHijri;
    Context context;
    private DateWrapperRemote dateRemote;
    private String TAG = "HomeFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(getLayoutInflater());


        initCalendar();
        selectDate();
        initListener();
        showEvent();
        convert();
        addEvent();


        return binding.getRoot();
    }

    private void addEvent() {
        binding.btnAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!binding.tvDateHijri.getText().toString().isEmpty()) {
                    NavDirections action = HomeFragmentDirections.actionHomeFragmentToMyDialog(dateHijri,gregorian);
                    Navigation.findNavController(binding.getRoot()).navigate(action);
                    binding.tvDateHijri.setText("");
                } else
                    Toast.makeText(getActivity(), "Choose Date Hijri First", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void convert() {
        binding.btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.tvSelectDate.getText().equals("select Date"))
                    getHijriDate();
                else
                    Toast.makeText(getActivity(), "Select Date", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showEvent() {
        binding.btnShowEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections action = HomeFragmentDirections.actionHomeFragmentToEventsFragment();
                Navigation.findNavController(binding.getRoot()).navigate(action);

            }
        });
    }

    private void initListener() {
        listener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                gregorian = day + "-" + month + "-" + year;
                binding.tvSelectDate.setText(getString(R.string.Gregorian) + gregorian);

            }
        };
    }

    private void selectDate() {
        binding.tvSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, listener, year, month, day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
    }

    private void initCalendar() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void getHijriDate() {

        HijriRemoteDao.getInstance().getData(gregorian).enqueue(result -> {
            switch (result.getStatus()) {
                case HttpStatus.SUCCESS:
                    Log.d(TAG, "getRemoteHijriDate: Success");

                    dateRemote = (result.getResult());
                    dateHijri = getString(R.string.hijri) + dateRemote.getData().getHijri().getDate();
                    binding.tvDateHijri.setText(dateHijri);
                    break;
                default:
                    Log.d(TAG, "not  Success  " + result.getCode() + result.getThrowable());
            }
        });

    }
}