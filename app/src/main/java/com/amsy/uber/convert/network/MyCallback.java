package com.amsy.uber.convert.network;


public interface MyCallback<T> {

    void onResult(HttpResult<T> result);

}
