package com.amsy.uber.convert.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.amsy.uber.convert.model.Event;

import java.util.List;


@Dao
public interface EventDao {
    @Insert
    void addNewEvent(Event event);

    @Delete
    void deleteEvent(Event Event);

    @Update
    void editEvent(Event Event);

    @Query("select * FROM Events")
    List<Event> getAllEvents();
    @Query("DELETE FROM Events")
    public void deleteAllEvents();
    @Query("SELECT * FROM Events WHERE id = :eventId")
    Event getEvent(int eventId);


}
