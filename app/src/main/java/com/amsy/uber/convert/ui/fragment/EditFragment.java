package com.amsy.uber.convert.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.amsy.uber.convert.databinding.FragmentEditBinding;
import com.amsy.uber.convert.model.Event;
import com.amsy.uber.convert.room.EventDao;
import com.amsy.uber.convert.room.MyApplication;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class EditFragment extends androidx.fragment.app.DialogFragment {


    FragmentEditBinding binding;
    private String date;
    private String event;
    private int id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        binding = FragmentEditBinding.inflate(getLayoutInflater());

        init();
        btnEdit();
        btnDelete();
        btnBack();
        return binding.getRoot();
    }

    private void btnBack() {
        binding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections action = EditFragmentDirections.actionDialogEditToEventsFragment();
                Navigation.findNavController(binding.getRoot()).navigate(action);


            }
        });
    }

    private void btnDelete() {
        binding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eventDao().deleteEvent(eventDao().getEvent(id));
                NavDirections action = EditFragmentDirections.actionDialogEditToEventsFragment();
                Navigation.findNavController(binding.getRoot()).navigate(action);

            }
        });
    }

    private void btnEdit() {
        binding.btnEditEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.btnEditEvent.setText("save");
                binding.etDescription.setEnabled(true);
                binding.etEventName.setEnabled(true);
                Event event = eventDao().getEvent(id);
                event.setEventDescription(binding.etDescription.getText().toString());
                event.setEventName(binding.etEventName.getText().toString());
                @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
                String date = df.format(Calendar.getInstance().getTime());
                event.setServerDatetime(date);

                eventDao().editEvent(event);


            }
        });
    }

    private void init() {
        date = EditFragmentArgs.fromBundle(getArguments()).getEvent();
        event = EditFragmentArgs.fromBundle(getArguments()).getDate();
        id = EditFragmentArgs.fromBundle(getArguments()).getId();
        binding.etEventName.setText(date);
        binding.etDescription.setText(event);
        binding.etEventName.setEnabled(false);
        binding.etDescription.setEnabled(false);
    }

    private EventDao eventDao() {
        return MyApplication.getInstanceDB().postDao();
    }
}