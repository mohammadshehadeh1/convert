package com.amsy.uber.convert.ui.fragment.events;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amsy.uber.convert.R;
import com.amsy.uber.convert.databinding.RowEventBinding;
import com.amsy.uber.convert.model.Event;
import com.amsy.uber.convert.room.EventDao;
import com.amsy.uber.convert.room.MyApplication;

import java.util.ArrayList;
import java.util.List;

class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private List<Event> list = new ArrayList<>();
    private OnRecycleViewItemClickListener listener;
    private int index = 0;
    private int idEvent;

    public EventsAdapter(List<Event> list, OnRecycleViewItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event, null, false);

        return new EventsAdapter.ViewHolder(RowEventBinding.bind(view));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Event event = list.get(position);

        holder.binding.tvDateHijri.setText(event.getDateHijri());
        holder.binding.tvEventDescription.setText(event.getEventDescription());
        holder.binding.dateAddPost.setText(event.getServerDatetime());
        holder.binding.tvEventName.setText(event.getEventName());
        holder.binding.tvDateG.setText(event.getGregorianDate());
        holder.binding.ivEdit.setTag(event.getId());


        showIVEdit(holder, position);


    }

    private void showIVEdit(@NonNull ViewHolder holder, int position) {
        holder.binding.cvEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = position;
                notifyDataSetChanged();
            }
        });

        if (index == position) {

            holder.binding.ivEdit.setVisibility(View.VISIBLE);
        } else {

            holder.binding.ivEdit.setVisibility(View.INVISIBLE);

        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RowEventBinding binding;

        public ViewHolder(RowEventBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    idEvent = (int) binding.ivEdit.getTag();
                    listener.Onclick(idEvent);
                }
            });


        }
    }

    private EventDao eventDao() {
        return MyApplication.getInstanceDB().postDao();
    }
}


