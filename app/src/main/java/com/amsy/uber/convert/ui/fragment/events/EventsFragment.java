package com.amsy.uber.convert.ui.fragment.events;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.amsy.uber.convert.R;
import com.amsy.uber.convert.databinding.FragmentEventsBinding;
import com.amsy.uber.convert.model.Event;
import com.amsy.uber.convert.room.EventDao;
import com.amsy.uber.convert.room.MyApplication;

import java.util.Collections;
import java.util.List;

public class EventsFragment extends Fragment {

    FragmentEventsBinding binding;
    private EventsAdapter adapter;
    private List<Event> list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventsBinding.inflate(getLayoutInflater());

        initAdapter();

        return binding.getRoot();
    }

    private void initAdapter() {
        list = eventDao().getAllEvents();
        Collections.reverse(list);

        adapter = (new EventsAdapter(list, eventId -> {
            String event = eventDao().getEvent(eventId).getEventDescription();
            String name = eventDao().getEvent(eventId).getEventName();
            NavDirections action = EventsFragmentDirections.actionEventsFragmentToDialogEdit2(event, name, eventId);
            Navigation.findNavController(binding.getRoot()).navigate(action);

        }));
        adapter.notifyDataSetChanged();
        binding.rvEvents.setAdapter(adapter);
    }




    private EventDao eventDao() {
        return MyApplication.getInstanceDB().postDao();
    }
}