package com.amsy.uber.convert.ui.dialog;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.amsy.uber.convert.R;
import com.amsy.uber.convert.databinding.FragmentDialogBinding;
import com.amsy.uber.convert.model.Event;
import com.amsy.uber.convert.room.EventDao;
import com.amsy.uber.convert.room.MyApplication;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class DialogFragment extends androidx.fragment.app.DialogFragment {


    FragmentDialogBinding binding;
    private String dateHijri;
    private String gregorian;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDialogBinding.inflate(getLayoutInflater());
        init();
        btnSave();
        btnBack();


        return binding.getRoot();
    }

    private void btnBack() {
        binding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void btnSave() {
        binding.btnSaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(binding.etWriteEvent.getText().toString().isEmpty()) && !(binding.etEventName.getText().toString().isEmpty())) {
                    addEventToRoom();
                    Toast.makeText(getActivity(), "Event Added", Toast.LENGTH_LONG).show();
                    binding.etWriteEvent.setText("");
                    binding.etEventName.setText("");
                   binding.btnBack.setText(R.string.done);
                } else
                    Toast.makeText(getActivity(), "Write Event and Name", Toast.LENGTH_SHORT).show();
            }

            private void addEventToRoom() {
                Event event = new Event();
                event.setDateHijri(dateHijri);
                event.setEventDescription(binding.etWriteEvent.getText().toString());
                event.setGregorianDate(binding.tvGregorianDate.getText().toString());
                @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
                String date = df.format(Calendar.getInstance().getTime());
                event.setServerDatetime(date);
                event.setEventName(binding.etEventName.getText().toString());
                eventDao().addNewEvent(event);
            }
        });
    }

    private void init() {
        dateHijri = DialogFragmentArgs.fromBundle(getArguments()).getDateHijri();
        gregorian = DialogFragmentArgs.fromBundle(getArguments()).getGregorian();
        binding.tvDateHijriDialog.setText(dateHijri);
        binding.tvGregorianDate.setText("Gregorian:" + gregorian);

    }

    private EventDao eventDao() {
        return MyApplication.getInstanceDB().postDao();
    }
}