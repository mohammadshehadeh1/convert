package com.amsy.uber.convert.model.Home;

import com.squareup.moshi.Json;

public class Weekday__1 {

    @Json(name = "en")
    private String en;

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

}

